# Myriad Trainer
## Introduction
The Myriad Model Trainer is a desktop application designed to ease the process of training machine learning models for Region Of Interest (ROI) detection applications.  The trainer is not required; Myriad the library and applications written using the library will run without its usage.  An example of training and saving an ROI model without using Trainer is available from the [Myriad Samples page](https://emphysic.com/myriad/sample-code/roi-detection-model-development/).

The basic workflow behind Myriad Trainer is as follows.

1. Gather training data representative of the ROI, including both positive (i.e. known to contain the ROI) and negative (i.e. known to not contain the ROI) samples.
2. Feed the training data to a prospective model to train it to detect ROI.
3. If the model is promising, save its current state for use in Myriad.
4. (Optional) Return to Step 1.

Documentation for Trainer is available from [Emphysic](https://gitlab.com/ccoughlin/myriad-docs) and [elsewhere](http://myrdocs.azurewebsites.net/trainer/).  A video demonstrating the basic usage of Trainer and its companion tool [Desktop](https://gitlab.com/ccoughlin/MyriadDesktop) is [also available](https://emphysic.wistia.com/medias/sbbft90a67).

## System Requirements
Trainer will run anywhere where Java 8 is available.  The current minimum requirements for Java 8 on the desktop are summarized below.

* Memory: 128MB RAM
* Disk: 126MB
* Processor: Pentium 2 266MHz or better

Full details on Java 8 requirements are available from [Oracle's Java site](http://www.oracle.com/technetwork/java/javase/certconfig-2095354.html) .  Myriad has been tested to compile and run on Windows 7, Windows 10, various Linux distributions, TrueOS, OpenBSD, and FreeBSD.

## Installation
The [Myriad library](https://gitlab.com/ccoughlin/datareader) must be installed to build Trainer.  Both are distributed as Apache Maven projects, full details on installation are available from the [myriad-docs project](https://gitlab.com/ccoughlin/myriad-docs/blob/master/docs/install.md).

[Desktop](https://gitlab.com/ccoughlin/MyriadDesktop) isn't required for Trainer.  Models created with Trainer can be used in Desktop or in other Myriad-based applications.

## Licensing
Copyright 2018 Emphysic, LLC.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.