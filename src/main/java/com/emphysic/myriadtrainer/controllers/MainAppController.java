/*
 * com.emphysic.myriadtrainer.controllers.MainAppController
 *
 * Copyright (c) 2018 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.emphysic.myriadtrainer.controllers;

import com.emphysic.myriad.core.data.io.Dataset;
import com.emphysic.myriad.core.data.ops.AbsoluteValueOperation;
import com.emphysic.myriad.core.data.ops.BinarizeOperation;
import com.emphysic.myriad.core.data.ops.BoxBlur;
import com.emphysic.myriad.core.data.ops.CannyOperation;
import com.emphysic.myriad.core.data.ops.ConvolutionOperation;
import com.emphysic.myriad.core.data.ops.DatasetOperation;
import com.emphysic.myriad.core.data.ops.DifferenceOfGaussiansOperation;
import com.emphysic.myriad.core.data.ops.FeatureScalingOperation;
import com.emphysic.myriad.core.data.ops.GaussianBlur;
import com.emphysic.myriad.core.data.ops.HOGOperation;
import com.emphysic.myriad.core.data.ops.NormalizeSignalOperation;
import com.emphysic.myriad.core.data.ops.PrewittOperation;
import com.emphysic.myriad.core.data.ops.ScalingOperation;
import com.emphysic.myriad.core.data.ops.ScharrOperation;
import com.emphysic.myriad.core.data.ops.SobelOperation;
import com.emphysic.myriad.core.data.roi.AdaptiveSGDROIFinder;
import com.emphysic.myriad.core.data.roi.MLROIFinder;
import com.emphysic.myriad.core.data.roi.PassiveAggressiveROIFinder;
import com.emphysic.myriad.core.data.roi.ROIFinder;
import com.emphysic.myriad.core.data.roi.SGDROIFinder;
import com.emphysic.myriad.core.data.util.FileSniffer;
import com.emphysic.myriad.core.experimental.roi.GradMachineROIFinder;
import com.emphysic.myriad.core.ml.CrossValidation;
import com.emphysic.myriadtrainer.controllers.MainAppController.preprocOp;
import com.emphysic.myriadtrainer.ui.MainApp;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ComboBoxModel;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import org.apache.commons.lang3.ArrayUtils;
import org.reflections.Reflections;
import lombok.extern.slf4j.Slf4j;
import org.apache.mahout.classifier.sgd.ElasticBandPrior;
import org.apache.mahout.classifier.sgd.L1;
import org.apache.mahout.classifier.sgd.L2;
import org.apache.mahout.classifier.sgd.PriorFunction;
import org.sgdtk.HingeLoss;
import org.sgdtk.LogLoss;
import org.sgdtk.Loss;
import org.sgdtk.SquaredHingeLoss;
import org.sgdtk.SquaredLoss;
import smile.math.kernel.GaussianKernel;
import smile.projection.KPCA;
import smile.projection.PCA;
import smile.projection.Projection;

/**
 * MainAppController - business logic for the Myriad Trainer Swing GUI
 * @author ccoughlin
 */
@Slf4j
public class MainAppController {
    /**
     * Handle to user interface JFrame
     */
    private JFrame frame;
   
    /**
     * Available preprocessing operations
     */
    public enum preprocOp {
        NONE,
        BUNDLED,
        ABSOLUTE,
        BINARIZE,
        BOX_BLUR,
        GAUSSIAN_BLUR,
        CANNY,
        CONVOLUTION,
        DOG,
        HOG,
        NORMALIZE,
        PREWITT,
        SCALING,
        SCHARR,
        SOBEL
    };
    
    /**
     * Class label balancing options
     * NONE - leave as-is (no balancing)
     * DOWN - rebalance by downsampling majority label
     * UP - rebalance by upsampling (SMOTE) minority label
     */
    public enum BalanceClasses{NONE, DOWN, UP};
    
    /**
     * Description of available preprocessing operations
     */
    private String[] preprocOpDescriptions = {
        "No preprocessing performed",
        "Using preprocessor bundled with serialized model (if any)",
        "Absolute Value",
        "Set all values to 0 or 1 based on a threshold",
        "Box blur",
        "Gaussian blur",
        "Canny Edge Detection",
        "User-defined convolution operation",
        "Difference of Gaussians Feature Enhancement",
        "Histogram of Oriented Gradients Object Detector",
        "Subtract minimum and divide by total range",
        "Prewitt Edge Detection",
        "Subtract mean and divide by standard deviation",
        "Scharr Edge Detection",
        "Sobel Edge Detection"
    };
    
    /**
     * Classes of available preprocessing operations
     */
    private String[] preprocOpClasses = {
        null,
        null,
        AbsoluteValueOperation.class.toString(),
        BinarizeOperation.class.toString(),
        BoxBlur.class.toString(),
        GaussianBlur.class.toString(),
        CannyOperation.class.toString(),
        ConvolutionOperation.class.toString(),
        DifferenceOfGaussiansOperation.class.toString(),
        HOGOperation.class.toString(),
        NormalizeSignalOperation.class.toString(),
        PrewittOperation.class.toString(),
        ScalingOperation.class.toString(),
        ScharrOperation.class.toString(),
        SobelOperation.class.toString()
    };
    
    /**
     * Combo box model for available preprocessing operations
     */
    private ComboBoxModel<preprocOp> preprocOperations = new DefaultComboBoxModel<>(preprocOp.values());
    
    /**
     * Current preprocessing operation
     */
    private DatasetOperation currentOp;
    
    /**
     * Current model
     */
    private MyriadModel currentModel;
    
    /**
     * Threshold for PCA function - datasets with more than BIGFEATURES features
     * use Kernel PCA, otherwise use standard PCA
     */
    private int BIGFEATURES = 1000;
    
    /**
     * Available machine learning model types
     */
    public enum model {
        SGD,
        ADAPTIVE_SGD,
        PASSIVE_AGGRESSIVE
    };
    
    public enum lossfunc {
        HINGE,
        LOG,
        SQUARED,
        SQUARED_HINGE
    };

    public MainAppController(JFrame app) {
        frame = app;
        initModel();
    }
    
    /**
     * Sets the preprocessing operation to be performed on data prior to 
     * sending it to the model.
     * @param type type of operation
     */
    //TODO: refactor here and in Desktop into separate lib
    public void setOperation(preprocOp type) {
        //currentOp = null;
        switch (type) {
            case ABSOLUTE:
                currentOp = new AbsoluteValueOperation();
                break;
            case BINARIZE:
                String t = JOptionPane.showInputDialog(frame,
                        "Enter a binarization threshold.",
                        "Configure Binarize Operation",
                        JOptionPane.OK_OPTION);
                try {
                    double threshold = Double.parseDouble(t);
                    currentOp = new BinarizeOperation(threshold);
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(frame, 
                            "Please enter a valid number for binarization threshold.",
                            "Invalid Threshold Specified",
                            JOptionPane.ERROR_MESSAGE);
                }
                break;
            case CANNY:
            case GAUSSIAN_BLUR:
            case BOX_BLUR:
                String r = JOptionPane.showInputDialog(frame,
                        "Enter a blur radius.",
                        "Configure Blur Operation",
                        JOptionPane.OK_OPTION);
                try {
                    int radius = Integer.parseInt(r);
                    switch (type) {
                        case GAUSSIAN_BLUR:
                            currentOp = new GaussianBlur(radius);
                            break;
                        case BOX_BLUR:
                            currentOp = new BoxBlur(radius);
                            break;
                        case CANNY:
                            currentOp = new CannyOperation(radius);
                            break;
                        default:
                            break;
                    }
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(frame,
                            "Please enter a valid number for blur radius.",
                            "Invalid Radius Specified",
                            JOptionPane.ERROR_MESSAGE);
                }
                break;
            case CONVOLUTION:
                String msg = "Enter a 2D convolution kernel separating columns with\n"
                        + "spaces or commas and rows by semi-colons e.g.\n" 
                        + "e.g. 1 2 3;4 5 6;7 8 9";
                String rawconv = JOptionPane.showInputDialog(frame,
                        msg,
                        "Configure Convolution Operation",
                        JOptionPane.OK_OPTION);
                try {
                    String[] rows = rawconv.split(";");
                    int colCount = rows[0].split("\\s*(,|\\s)\\s*").length;
                    double[][] kernel = new double[rows.length][colCount];
                    for (int i=0; i<rows.length; i++) {
                        String[] els = rows[i].split("\\s*(,|\\s)\\s*");
                        for (int j=0; j<colCount; j++) {
                            kernel[i][j] = Double.parseDouble(els[j]);
                        }
                    }
                    currentOp = new ConvolutionOperation(kernel);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(frame,
                            "Unable to create kernel:\n" + e.getLocalizedMessage(),
                            "Invalid Kernel Specified",
                            JOptionPane.ERROR_MESSAGE);
                }
                break;
            case DOG:
                String rawdog = JOptionPane.showInputDialog(frame,
                        "Enter two blur radii separated by a space or comma e.g. '1 2' or '3,4'.",
                        "Configure Difference of Gaussians Operation",
                        JOptionPane.OK_OPTION);
                try {
                    int r1;
                    int r2;
                    String[] toks = rawdog.split("\\s*(,|\\s)\\s*");
                    r1 = Integer.parseInt(toks[0]);
                    r2 = Integer.parseInt(toks[1]);
                    currentOp = new DifferenceOfGaussiansOperation(r1, r2);
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(frame,
                            "Please enter valid numbers for blur radius.",
                            "Invalid Radius Specified",
                            JOptionPane.ERROR_MESSAGE);
                }
                break;
            case NORMALIZE:
                currentOp = new NormalizeSignalOperation();
                break;
            case SCALING:
                currentOp = new ScalingOperation();
                break;
            case SCHARR:
                currentOp = new ScharrOperation();
                break;
            case SOBEL:
                currentOp = new SobelOperation();
                break;
            case PREWITT:
                currentOp = new PrewittOperation();
                break;
            case HOG:
                String rawhog = JOptionPane.showInputDialog(frame,
                        "Enter a cell radius and a block size separated by a space or comma e.g. '1 2' or '1,2'.",
                        "Configure HOG Operation",
                        JOptionPane.OK_OPTION);
                try {
                    int cellRadius;
                    int blockSize;
                    String[] toks = rawhog.split("\\s*(,|\\s)\\s*");
                    cellRadius = Integer.parseInt(toks[0]);
                    blockSize = Integer.parseInt(toks[1]);
                    currentOp = new HOGOperation(cellRadius, blockSize, 9);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(frame,
                            "Please enter valid numbers.",
                            "Invalid Numbers Specified",
                            JOptionPane.ERROR_MESSAGE);
                }
                break;
            case BUNDLED:
                if (currentOp != null) {
                    preprocOpDescriptions[1] = "Bundled: " + currentOp.getClass().toString();
                }
                break;
            case NONE:
            default:
                currentOp = null;
                break;
        }
    }
    
    /**
     * @return the frame
     */
    public JFrame getFrame() {
        return frame;
    }

    /**
     * @param frame the frame to set
     */
    public void setFrame(JFrame frame) {
        this.frame = frame;
    }

    /**
     * @return the preprocOperations
     */
    public ComboBoxModel<preprocOp> getPreprocOperations() {
        return preprocOperations;
    }

    /**
     * @return the currentOp
     */
    public DatasetOperation getCurrentOp() {
        return currentOp;
    }

    /**
     * @param currentOp the currentOp to set
     */
    public void setCurrentOp(DatasetOperation currentOp) {
        this.currentOp = currentOp;
    }

    /**
     * @return the currentModel
     */
    public MyriadModel getCurrentModel() {
        return currentModel;
    }

    /**
     * @param currentModel the currentModel to set
     */
    public void setCurrentModel(MyriadModel currentModel) {
        this.currentModel = currentModel;
    }
    
    /**
     * Reads a model from disk
     * @param inFile model to read
     */
    public void setCurrentModel(File inFile) {
        Reflections reflections = new Reflections("com.emphysic.myriad");
        Set<Class<? extends MLROIFinder>> subTypes = reflections.getSubTypesOf(MLROIFinder.class);
        subTypes.stream().forEach((clz) -> {
            try {
                currentModel.setModel((MLROIFinder)ROIFinder.fromFile(inFile, clz));
                log.info("Read " + inFile + " as " + clz);
                
            } catch (Exception e) {
                log.info("Couldn't read " + inFile + " as " + clz + ": " + e.getMessage());
            }
        });
    }
    
    /**
     * Instantiates a new SGD model based on the current settings
     * @param loss loss function to use one of "hinge" "log" "squared" or "squared hinge" defaults to "hinge"
     * @param regularization regularization factor
     * @return new SGD model instance, or null if a problem occurred
     */
    public MLROIFinder buildSGDModel(String loss, Double regularization) {
        Loss lossfunction;
        if (loss == null || loss.isEmpty()) {
            lossfunction = new HingeLoss();
        } else {
            switch (loss.toLowerCase(Locale.getDefault())) {
                case "hinge":
                    lossfunction = new HingeLoss();
                    break;
                case "log":
                    lossfunction = new LogLoss();
                    break;
                case "squared":
                    lossfunction = new SquaredLoss();
                    break;
                case "squared hinge":
                    lossfunction = new SquaredHingeLoss();
                    break;
                default:
                    lossfunction = null;
                    break;
            }
        }
        if (lossfunction != null) {
            return new SGDROIFinder(lossfunction, regularization);
        }
        return null;
    }
    
    /**
     * Instantiates a new AdaptiveSGD model based on the current settings
     * @param reg specifies regularization function, one of "l1" "l2" "elastic" . Defaults to no-param l1.
     * @param ct confidence threshold - ROI labels with less than this probability are re-labeled 
     * @param numcores number of cores to use 
     * @param nummodels number of models to train
     * @return new AdaptiveSGD model instance, or null if a problem occurred
     */
    public MLROIFinder buildAdaptiveSGDModel(String reg, Double ct, Integer numcores, Integer nummodels) {
        PriorFunction regFunction = null;
        String message;
        if (reg == null) {
            regFunction = new L1();
        } else {
            switch (reg.toLowerCase(Locale.getDefault())) {
                case "l1":
                    regFunction = new L1();
                    break;
                case "l2":
                    message = JOptionPane.showInputDialog(frame, 
                            "Please enter a scaling factor between 0 and 1.");
                    double scale;
                    try {
                        scale = Double.parseDouble(message);
                        assert(scale > 0);
                        assert(scale <= 1);
                    } catch (NumberFormatException | AssertionError e) {
                        scale = 0.5;
                        Logger.getLogger(MainApp.class.getName()).log(Level.WARNING, 
                            "Invalid L2 scale specified - using 0.5");
                    }
                    regFunction = new L2(scale);
                    break;
                case "elastic":
                    message = JOptionPane.showInputDialog(frame, 
                            "Please enter a scaling factor for L2 between 0 and 1.");
                    double l2scaler;
                    try {
                        l2scaler = Double.parseDouble(message);
                        assert(l2scaler > 0);
                        assert(l2scaler <= 1);
                    } catch (NumberFormatException | AssertionError e) {
                        l2scaler = 0.5;
                        Logger.getLogger(MainApp.class.getName()).log(Level.WARNING, 
                            "Invalid Elastic scale specified - using 0.5");
                    }
                    regFunction = new ElasticBandPrior(l2scaler);
                    break;
                default:
                    break;
            }
        }
        AdaptiveSGDROIFinder mdl = null;
        if (regFunction != null) {
            mdl = new AdaptiveSGDROIFinder(2, regFunction, numcores, nummodels);
            mdl.setConfidenceThreshold(ct);
        }
        return mdl;
    }
    
    /**
     * Returns a new PassiveAggressive model instance based on current
     * settings.
     * @param learningRate learning rate (smaller = more iterations)
     * @param ct confidence threshold - ROI labels with less than this probability are re-labeled 
     * @return PassiveAggressive model, or null if an error occurred.
     */
    public MLROIFinder buildPassiveAgressiveModel(Double learningRate, Double ct) {
        PassiveAggressiveROIFinder model = null;
        try {
            model = new PassiveAggressiveROIFinder(learningRate);
        } catch (NumberFormatException e) {
            Logger.getLogger(MainApp.class.getName()).log(Level.WARNING, 
                "Invalid passive aggressive learning rate specified - using 0.1");
        }
        if (model != null) {
            try {
                model.setConfidenceThreshold(ct);
            } catch (IllegalArgumentException e) {
                // Didn't enter a number or entered one less than 0 or greater than 1
                JOptionPane.showMessageDialog(frame,
                    "Please enter a valid number between 0 and 1.",
                    "Invalid Confidence Threshold",
                    JOptionPane.ERROR_MESSAGE);
                Logger.getLogger(MainApp.class.getName()).log(Level.WARNING, 
                        "Invalid confidence threshold of {0} specified.", 
                        ct);
                return null;
            }
        }
        return model;
    }
    
    /**
     * Returns a new GradMachineROIModel.
     * @param numCats number of categories
     * @param learningRate learning rate
     * @param regularization regularization 
     * @param numHidden number of nodes in the hidden layer.
     * @param sparsity sparsity of hidden layer
     * @param ct confidence threshold
     * @return  new model
     */
    public MLROIFinder buildGradientMachineModel(
            int numCats, 
            double learningRate, 
            double regularization, 
            int numHidden, 
            double sparsity,
            double ct) {
        GradMachineROIFinder model = new GradMachineROIFinder(
                numCats, numHidden, learningRate, sparsity, regularization
        );
        model.setConfidenceThreshold(ct);
        return model;
    }
    
    /**
     * Initializes the ROI model - current model in controller is replaced with
     * a new untrained model.
     *
     * @param model new model
     */
    public void initModel(MLROIFinder model) {
        initModel();
        currentModel.setModel(model);
        currentModel.setAccuracy(0);
    }
    
    /**
     * Initializes the current model
     */
    public void initModel() {
        this.currentModel = new MyriadModel();
    }
    
    /**
     * Returns a description of a preprocessing operation
     * @param idx index of the operation
     * @return tool tip description
     */
    public String getOpTip(int idx) {
        return preprocOpDescriptions[idx];
    }
    
    /**
     * Converts a DatasetOperation class name to the equivalent enum
     * @param className name of class
     * @return value in preprocOp enum
     */
    public preprocOp getOpType(String className) {
        return preprocOp.values()[ArrayUtils.indexOf(preprocOpClasses, className)];
    }
    
    /**
     * Reads all the Datasets from a given file folder.
     *
     * @param folder folder to load
     * @return List of all Datasets load from specified folder
     * @throws Exception if an I/O error occurs
     */
    public List<Dataset> readFolder(File folder) throws Exception {
        File[] files = folder.listFiles();
        assert (files != null);
        List<Dataset> datasets = new ArrayList<>();
        for (File f : files) {
            Dataset d = FileSniffer.read(f, true);
            if (d != null) {
                datasets.add(d);
            } else {
                log.warn("Unable to read file '" + f + "', skipping");
            }
        }
        return datasets;
    }
    
    /**
     * Compiles positive and negative samples into a train/test data set.
     *
     * @param positiveSamples positive samples
     * @param negativeSamples negative samples
     * @param normalize if true, normalize data between 0 and 1 prior to running any additional preprocessing operation
     * @return compiled data, or null if no positive/negative samples, couldn't
     * determine model, etc.
     */
    public CrossValidation.Data compileData(
            List<Dataset> positiveSamples, 
            List<Dataset> negativeSamples, 
            boolean normalize) {
        Random rnd = new Random();
        List<Dataset> samples = new ArrayList<>();
        List<Integer> sampleLabels = new ArrayList<>();
        if (positiveSamples == null || positiveSamples.isEmpty()) {
            log.warn("No positive samples found.");
            return null;
        }
        if (negativeSamples == null || negativeSamples.isEmpty()) {
            log.warn("No negative samples found.");
            return null;
        }
        NormalizeSignalOperation norm = new NormalizeSignalOperation();
//        switch (balance) {
//            case DOWN:
//                int sampleSize = Math.min(positiveSamples.size(), negativeSamples.size());
//                while (positiveSamples.size() != sampleSize) {
//                    positiveSamples.remove(rnd.nextInt(positiveSamples.size()));
//                }
//                while (negativeSamples.size() != sampleSize) {
//                    negativeSamples.remove(rnd.nextInt(negativeSamples.size()));
//                }
//                break;
//            case UP:
//                
//        }
        int posClass = (int)getCurrentModel().getModel().positiveClass();
        int negClass = (int)getCurrentModel().getModel().negativeClass();
        Collections.shuffle(positiveSamples, rnd);
        Collections.shuffle(negativeSamples, rnd);
        samples.addAll(positiveSamples);
        samples.addAll(negativeSamples);
        DatasetOperation preprocessor = getCurrentOp();
        if (preprocessor instanceof FeatureScalingOperation) {
            ((FeatureScalingOperation) preprocessor).fit(samples);
        }
        positiveSamples.forEach((_item) -> {
            sampleLabels.add(posClass);
        });
        negativeSamples.forEach((_item) -> {
            sampleLabels.add(negClass);
        });
        double[][] X = new double[samples.size()][samples.get(0).getSize()];
        int[] y = new int[sampleLabels.size()];
        for (int i = 0; i < samples.size(); i++) {
            Dataset currentDataset = samples.get(i);
            if (normalize) {
                currentDataset = norm.run(currentDataset);
            }
            if (preprocessor != null) {
                currentDataset = preprocessor.run(currentDataset);
            }
            X[i] = currentDataset.getData();
            y[i] = sampleLabels.get(i);
        }
        return new CrossValidation.Data(X, y);
    }
    
    /**
     * Creates a PCA Projection from the specified data. By default a standard
     * linear PCA is used, however if the data has more than BIGFEATURES
     * features a kernel PCA is used instead as a faster / smaller alternative.
     *
     * @param data data to project
     * @return PCA Projection
     */
    public Projection getProjection(CrossValidation.Data data) {
        Projection pca = null;
        if (data != null && data.samples != null) {
            StringBuilder sb = new StringBuilder();
            int numFeatures = data.samples[0].length;
            sb.append("Found ").append(data.samples.length).append(" samples with ").append(numFeatures).append(" features, ");
            if (numFeatures > BIGFEATURES) {
                pca = new KPCA(data.samples, new GaussianKernel(1), 3);
                sb.append("using kernel PCA");
            } else {
                pca = new PCA(data.samples);
                ((PCA) pca).setProjection(3);
                sb.append("using conventional PCA");
            }
            log.info(sb.toString());
        } else {
            log.info("No data / samples supplied for PCA!");
        }
        return pca;
    }
    
    /**
     * Returns the current threshold for "big" feature spaces - anything over 
     * this number of features uses kernel PCA instead of conventional PCA
     * @return 
     */
    public int getFeatureSizeThreshold() {
        return BIGFEATURES;
    }
    
    /**
     * Sets the threshold for "big" feature spaces - anything over this number
     * of features uses kernel PCA instead of conventional PCA
     * @param t new threshold
     */
    public void setFeatureSizeThreshold(int t) {
        BIGFEATURES = t;
    }
    
    public static class MyriadModel {
        private MLROIFinder model;
        private double accuracy;
        
        public MyriadModel(){}
        public MyriadModel(MLROIFinder model, double accuracy) {
            this.model = model;
            this.accuracy = accuracy;
        }
        
        public MyriadModel(MLROIFinder model) {
            this(model, 0);
        }
        
        public MLROIFinder getModel() { return model;}
        public void setModel(MLROIFinder mdl) { this.model = mdl;}
    
        public double getAccuracy() { return accuracy;}
        public void setAccuracy(double accuracy) { this.accuracy = accuracy;}
    }
}
