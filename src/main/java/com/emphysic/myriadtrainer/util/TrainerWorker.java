/*
 * com.emphysic.myriadtrainer.util.TrainerWorker
 *
 * Copyright (c) 2018 Emphysic LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.emphysic.myriadtrainer.util;

import com.emphysic.myriad.core.data.roi.MLROIFinder;
import com.emphysic.myriad.core.ml.CrossValidation;
import com.emphysic.myriad.core.ml.MonteCarloCV;
import java.util.Map;
import javax.swing.SwingWorker;
import lombok.extern.slf4j.Slf4j;

/**
 * TrainerWorker - SwingWorker for training machine learning-based Region of
 * Interest (ROI) detectors
 * @author ccoughlin
 */
@Slf4j
public class TrainerWorker extends SwingWorker<MLROIFinder, Void> {
    /**
     * Fraction of data to use as training e.g. 0.75, remainder for
     * testing
     */
    private double trainRatio;
    /**
     * Data to use for train/test
     */
    private CrossValidation.Data data;
    /**
     * The initial model to train and test
     */
    private MLROIFinder initialModel;
    
    private MonteCarloCV cv;
    /**
     * Current accuracy of the model
     */
    private double accuracy;
    
    /**
     * Number of rounds of training (defaults to 5)
     */
    private int iters = 5;
    
    /**
     * Whether to attempt to balance minority / majority labels in the training
     * set with upsampling
     */
    private boolean upSample = true;
    
    /**
     * Constructor
     * @param model model to train
     * @param data data used to train the model
     * @param trainRatio proportion of data to use for training, 1-trainRatio is used for testing.
     * @param rounds number of rounds of train/test to perform
     * @param upSample if true, attempt to balance training set via upsampling
     */
    public TrainerWorker(MLROIFinder model, 
            CrossValidation.Data data, 
            double trainRatio,
            int rounds,
            boolean upSample) {
        this.initialModel = model;
        this.data = data;
        this.trainRatio = trainRatio;
        this.iters = rounds;
        this.cv = new MonteCarloCV(this.data, trainRatio);
    }
    
        /**
     * Constructor
     * @param model model to train
     * @param data data used to train the model
     * @param trainRatio proportion of data to use for training, 1-trainRatio is used for testing.
     * @param rounds number of rounds of train/test to perform
     */
    public TrainerWorker(MLROIFinder model, 
            CrossValidation.Data data, 
            double trainRatio,
            int rounds) {
        this(model, data, trainRatio, rounds, true);
    }
    
    /**
     * Constructor - uses 75% of data as train and 25% for test, conducts 5 rounds.
     * @param model model to train
     * @param data data used to train the model
     */
    public TrainerWorker(MLROIFinder model, CrossValidation.Data data) {
        this(model, data, 0.75, 5, true);
    }
    
    @Override
    protected MLROIFinder doInBackground() throws Exception {
        try {
            if (isCancelled()) {
                throw new InterruptedException();
            }
            // Sort of a fudge here - we're not really doing CV just looking for accuracy metrics
            Map.Entry<MLROIFinder, Double> bestSGDModel = cv.findBestModel(iters, 
                    new MLROIFinder[] {initialModel},
                    upSample
            );
            if (isCancelled()) {
                throw new InterruptedException();
            }
            accuracy = bestSGDModel.getValue() * 100;
            log.info("Found best model accuracy " + accuracy + "%");
            return bestSGDModel.getKey();
        } catch (InterruptedException e) {
            log.info("Interrupted");
            Thread.currentThread().interrupt();
        }
        return null;
    }

    /**
     * @return the trainRatio
     */
    public double getTrainRatio() {
        return trainRatio;
    }

    /**
     * @param trainRatio the trainRatio to set
     */
    public void setTrainRatio(double trainRatio) {
        this.trainRatio = trainRatio;
    }

    /**
     * @return the data
     */
    public CrossValidation.Data getData() {
        return data;
    }

    /**
     * @param data the data to set
     */
    public void setData(CrossValidation.Data data) {
        this.data = data;
    }

    /**
     * @return the model
     */
    public MLROIFinder getModel() {
        return initialModel;
    }

    /**
     * @param model the model to set
     */
    public void setModel(MLROIFinder model) {
        this.initialModel = model;
    }

    /**
     * @return the accuracy
     */
    public double getAccuracy() {
        return accuracy;
    }

    /**
     * @param accuracy the accuracy to set
     */
    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }

    /**
     * @return the iters
     */
    public int getIters() {
        return iters;
    }

    /**
     * @param iters the iters to set
     */
    public void setIters(int iters) {
        this.iters = iters;
    }
    
    public CrossValidation getCV() {
        return cv;
    }
}
